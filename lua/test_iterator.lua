local function for_it()
    local arr = {}
    for i = -2, 2 do
        arr[i] = i
    end

    -- 遍历所有键值对
    for index, value in pairs(arr) do
        print(index, value)
    end

    -- 遍历key为正整数的键值对
    for key, value in ipairs(arr) do
        print(key, value)
    end
end

local function main()
    for_it()
end

main()
