#!/usr/bin/env lua
-- 遍历当前目录下所有lua文件

local function findFilesInCurrentDir()
    -- 通过io.popen调用命令行查找当前目录下所有lua文件
    local findCommand = "find . -type f -name '*.lua'"
    local processHandle = io.popen(findCommand, "r")
    if not processHandle then
        return {}
    end

    local fileList = {}
    for filename in processHandle:lines() do
        -- 通过正则表达式提取文件名中以'/'分割的最后一个子串，即文件的不含路径的basename
        local basename = string.match(filename, "[^/]+$")
        if basename then
            table.insert(fileList, basename)
        end
    end

    if not processHandle then
        processHandle:close()
    end

    return fileList
end


local fileList = findFilesInCurrentDir()
for _, file in pairs(fileList) do
    print(file)
end
