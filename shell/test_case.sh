#!/usr/bin/bash
#测试shell中的case语法

test01() {
	case $1 in
	1) echo "input 1" ;;
	2) echo "input 2" ;;
	*) echo "$1" ;;
	esac
}

test02() {
    # 当函数输入参数大于0的时,执行如下循环
	while [ $# -gt 0 ]; do
		case "$1" in
		1)
			echo "hello"
            #将输入参数移动到下一位置，即将$(n)变为$(n+1)
            #shift+n 表示移动n位
			shift
			;;
		2)
			echo "lua"
			shift
			;;
		*)
			echo "nothing"
			break
			;;
		esac
	done
}

test01 3
test02 1 2 3