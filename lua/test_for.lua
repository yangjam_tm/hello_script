#!/usr/bin/env lua
--[[
测试for循环语法，主要分为两种:
1. 数值循环,语法如下,从`start`遍历到`end`,`step`为cnt每次迭代的间隔,为可选值
    for cnt = start, end, [step] do
        do_something
    end
2. 泛型循环,用于迭代容器,即table
--]]

local function test01()
    -- 从1遍历到10,间隔为1
    for i = 1, 10 do
        io.write(i, '\t')
    end
    print()
    -- output: 1, 2, 3, ..., 10
end

local function test02()
    -- 从10遍历到1,间隔为-2
    for i = 10, 1, -2 do
        io.write(i, ' ')
    end
    print()
end

local function test03()
    local target = { a1 = 'hello', b1 = 234, c3 = true }
    -- 通过"迭代器"pairs遍历target
    -- pairs返回三个值：索引（key）、值（value）以及一个布尔值，指示是否还有更多的元素可以遍历
    for key, val in pairs(target) do
        print(key, val)
    end
end


local function main()
    test01()
    test02()
    test03()
end


main()
