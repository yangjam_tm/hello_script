#!/usr/bin/lua
-- 遍历当前目录及其子目录下所有文件，并打印其文件属性

local lfs = require("lfs")

local function attrdir(path)
    -- 遍历当前目录下所有文件
    for file in lfs.dir(path) do
        -- 排除本目录及上级目录
        if file ~= "." and file ~= ".." then
            local f = path .. '/' .. file
            print("\t " .. f)
            -- 读取文件属性
            local attr = lfs.attributes(f)
            assert(type(attr) == "table")
            if attr.mode == "directory" then -- 如果是子目录，就递归执行
                attrdir(f)
            else
                -- 遍历文件的所有属性
                for name, value in pairs(attr) do
                    print(name, value)
                end
            end
        end
    end
end

attrdir(".")
