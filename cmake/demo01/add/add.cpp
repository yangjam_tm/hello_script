#include "add.h"

template <>
int CAdd<int>::operator()(const int& a, const int& b)
{
    return a + b;
}