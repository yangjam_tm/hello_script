#!/usr/bin/lua
-- 测试lua循环语法

total = 10

function loop1()
    -- local total = 10
    local i = 0
    while i < total do
        -- print(i, '\t')
        io.write(i, '\t')
        i = i + 1
    end
    print('while-loop end')
end

function loop2()
    -- local total = 10
    local step = 2
    for i = 0, total, step do
        -- print(i, '\t')
        io.write(i, '\t')
        -- i = i + 1
    end
    print('for-loop end')
end

function loop3()
    -- local total = 10
    local i = 0
    repeat
        -- print(i, '\t')
        io.write(i, '\t')
        i = i + 1
    until i > total
    print("repeat-loop end")
end

-- 使用要在定义之后
loop1()
loop2()
loop3()
