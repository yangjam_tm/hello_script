```makefile
target_file = $(obj)
target = $(basename $(target_file))

mem_check := -fsanitize=address
commom_flags := -g -Wall $(mem_check) -I../include
# 启用终端颜色
extra_flags := -fdiagnostics-color=auto
ifeq ($(suffix $(target_file)), .cpp)
    CC := g++
    cflags := -std=c++11
else
    CC := gcc
    cflags := -std=c11
endif
cflags += $(commom_flags) $(extra_flags)

$(target): $(target_file)
	$(CC) $< -o $@ $(cflags)
```