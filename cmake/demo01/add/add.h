#ifndef __ADD_H__
#define __ADD_H__

template <typename T>
class CAdd{
public:
    T operator()(const T& a, const T& b);
};

#endif