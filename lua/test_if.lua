#!/usr/bin/lua
--[[
测试lua中if-else语法:
if condition then
    do something
else
    do otherthing
end
--]]

function max(a, b)
    -- if条件后面需要 `then` 而 else 后面不需要 `then`
    if a > b then
        return a
    else
        return b
    end
end

-- lua中，只有 `nil` 和 `false` 为 false，其它均为 true
function test_bool()
    if 0 then
        print('0 is true')
    end
    if not nil then
        print('nil is false')
    end
end

function multi_if()
    a = 123
    if a < 10 then
        print(a * 10)
        -- lua不支持 else if；但是可以在if和else中再嵌套if-else实现嵌套或多条件判断
    else
        if a > 100 then
            print(a / 10) -- output: 12.3 lua中 `/` 为数学意义上除法，整除为 `//`
        else
            print(a)
        end
    end
end
print(max(4, 6))
test_bool()
multi_if()
