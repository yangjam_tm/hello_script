```makefile
target_file = $(obj)
target = $(basename $(target_file))

commom_flags := -g -Wall
cv_flags = `pkg-config --cflags --libs opencv4`

CC := g++
cflags := -std=c++11

cflag += $(commom_flags) $(cv_flags)

$(target): $(target_file)
	$(CC) $< -o $@ $(cflag)
```