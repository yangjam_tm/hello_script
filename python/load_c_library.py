#!/usr/bin/env python3
""" 加载c/c++动态库,调用c/c++接口
"""

from ctypes import *
import platform

system = platform.system()
def run_linux_example():
    # 使用cdll加载
    # libc = cdll.LoadLibrary('libc.so.6')
    # or使用CDLL进行构造
    libc = CDLL('libc.so.6')
    print(libc)
    print(libc.rand())
def run_windows_example():
    print(windll.kernel32)

def load_test():
    libtest = CDLL('./test.so')
    print(libtest.add(c_int(12), c_int(14)))

    # 编译c++库时，需要将接口封装为c接口，python才能正常加载
    libetst_cpp = CDLL("./test_cpp.so")
    print(libetst_cpp.add(c_int(11), c_int(19)))

if __name__ == '__main__':
    if system == 'Linux':
        run_linux_example()
        load_test()
    elif system == 'Windows':
        run_windows_example()
    else:
        print("unkown system")
