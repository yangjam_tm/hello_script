#!/usr/bin/lua

-- lua中不存在数组，而是使用table模拟array

local function default_array()
    local arr = { 1, 2, 3, 4, 5, 6 }
    -- 默认序号从1开始
    for i = 1, #arr do
        io.write(arr[i], '\t')
    end
    print()
end

local function user_indexs()
    local arr = {}
    -- 自定义序号，从-2开始，实质是自定义key-value的映射关系
    for i = -2, 3 do
        arr[i] = i * 2
    end
    for i = 1, #arr do
        io.write(arr[i], '\t')
    end
    io.write('\n')
end

local function multi_array()
    local arr = {}
    arr[1] = { 1, 2, 3 }
    arr[2] = { 4, 5, 6 }
    arr[3] = { 7, 8 }

    for row = 1, #arr do
        local max_cols = #arr[row]
        for col = 1, max_cols do
            io.write(arr[row][col], '\t')
        end
    end
end


local function main()
    default_array()
    user_indexs()
    multi_array()
end

main()
