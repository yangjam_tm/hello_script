#!/usr/bin/lua

local str1 = 'Hello'
local str2 = "lua!"
local str3 = [[
    This is a long string that spans
    across multiple lines
    Hello, Lua!
]]

print(str1)      -- 输出：Hello
-- `..` 连接字符串
print(str1 .. ' ' .. str2) -- 输出：Hello lua!
-- `#` 获取字符串长度
print(#str3, str3)