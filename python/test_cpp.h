#ifndef TEST_CPP_H
#define TEST_CPP_H

// 使用C接口声明，c++添加的修饰，ctypes无法识别
extern "C"
{
    int add(int a, int b);
}

#endif // end TEST_CPP_H