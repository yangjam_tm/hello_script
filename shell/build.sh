#!/bin/bash
# c/c++单文件编译脚本
# 读取命令行输入或选择最新修改的文件，使用make命令进行编译
build_file=""
if [[ -z $1 ]]&&[[ -e $1 ]]
then
    build_file=$1
else
    # ls -t 将文件按照时间进行排序
    # grep -E ".c|.cpp" 选择后缀为c或cpp的文件
    # head -n 1 只输出第一行
    build_file=$(ls -t | grep -E ".c|.cpp" | head -n 1)
fi
if [ -e ${build_file} ]
then
    echo "build ${build_file}"
    make obj=${build_file}
fi
run_file=${build_file%.*}
if [ -e ${run_file} ]
then
    echo "------ run ${run_file} ------"
    ./${run_file}; rm ${run_file}
fi